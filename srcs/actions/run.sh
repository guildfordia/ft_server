#!/bin/sh

# GO TO ROOT DIRECTORY
cd ../..

# BUILD IMAGE
docker build -t ft_server .

# RUN CONTAINER
docker run -d -p 8888:80 -p 443:443 \
--name server1 -it ft_server
