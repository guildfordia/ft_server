# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Dockerfile                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: antoine <antoine@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/09 22:29:39 by antoine           #+#    #+#              #
#    Updated: 2020/12/09 22:29:47 by antoine          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FROM debian:buster

# UPDATE AND UPGRADE
RUN	apt update -y && \
	apt upgrade -y

# INSTALL WGET
RUN	apt-get install -y wget

# INSTALL NGINX
RUN	apt-get install -y nginx

# INSTALL MYSQL DATABASE
RUN	apt-get install -y mariadb-server

# INSTALL PHP
RUN	apt-get install -y php \
	php-mysql \
	php-cli \
	php-mbstring \
	php-fpm \
	php-gettext

# ALLOW NGINX USER TO EDIT ALL FILES IN WWW
RUN	chown -R www-data /var/www/* && \
	chmod -R 755 /var/www/* 

# CREATE SERVER DIRECTORY
RUN	mkdir /var/www/ft_server

# INSTALL PHPMYADMIN
RUN	mkdir /var/www/ft_server/phpmyadmin && \
	wget https://files.phpmyadmin.net/phpMyAdmin/5.0.4/phpMyAdmin-5.0.4-all-languages.tar.gz && \
	tar -zxvf phpMyAdmin-5.0.4-all-languages.tar.gz --strip-components 1 \
	-C /var/www/ft_server/phpmyadmin  && \
	rm phpMyAdmin-5.0.4-all-languages.tar.gz

# SETUP MYSQL DATABASE
RUN	service mysql start && \
	mysql -u root -e "CREATE DATABASE wordpress;" && \
	mysql -u root -e "GRANT ALL PRIVILEGES ON wordpress.* TO 'root'@'localhost';" && \
	mysql -u root -e "update mysql.user set plugin='mysql_native_password' where user='root';" && \
	mysql -u root -e "FLUSH PRIVILEGES;"
	
# INSTALL WORDPRESS
COPY	srcs/config/wordpress.tar.gz /var/www/ft_server
RUN	cd /var/www/ft_server && \
	tar -zxvf wordpress.tar.gz && \
	rm wordpress.tar.gz

# COPY FILES IN SRCS IN THE RIGHT FOLDERS
COPY	srcs/config/phpmyadmin-config.inc.php /var/www/ft_server/phpmyadmin/config.inc.php
COPY	srcs/config/wp-config.php /var/www/ft_server/wordpress
COPY	srcs/config/nginx.conf /etc/nginx/sites-available/ft_server
COPY	srcs/config/autoindex /usr/bin

# GIVE AUTOINDEX PERMISSIONS
RUN	chmod +x /usr/bin/autoindex

# CONFIGURE NGINX
RUN	ln -s /etc/nginx/sites-available/ft_server /etc/nginx/sites-enabled/ft_server && \
	rm -rf /etc/nginx/sites-enabled/default

# CREATE SELF-SIGNED SSL CERTIFICATE
RUN	openssl req -x509 -nodes -days 365 \
	-subj "/C=FR/ST=Paris/L=Paris/O=42/OU=alangloi/CN=localhost" -newkey rsa:2048 \
	-keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt

# START SERVICE AND CREATE LOOP
CMD	service nginx start && \
	service mysql start && \
	service php7.3-fpm start && \
	tail -f /dev/null

# OPEN PORTS
EXPOSE	80 443
